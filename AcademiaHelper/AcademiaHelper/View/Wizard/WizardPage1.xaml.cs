﻿/*
Copyright 2013 Rodrigo Cabral

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using AcademiaHelper.Model;
using AcademiaHelper.ViewModel;
using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Linq;
using Microsoft.Phone.Shell;
using AcademiaHelper.Helpers;
using System.Windows.Navigation;

namespace AcademiaHelper.View.Wizard
{
    public partial class WizardPage1 : PhoneApplicationPage
    {
        private int ListPickerSerieSelectedIndex { get; set; }
        private int ListPickerExercicioSelectedIndex { get; set; }

        private bool Refresh { get; set; }

        public WizardPage1()
        {
            InitializeComponent();
            this.DataContext = App.WizardViewModel;
        }

        private void imgBtnAddSerie_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(Paginas.Add_Serie);
        }

        private void imgBtnAddExercicio_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(Paginas.Add_Exercico);
        }


        private void txtRepeticaoDaSerie_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            AcademiaHelper.Helpers.Util.FormatarEntradaDeNumeros((TextBox)sender, false, false);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            this.Refresh = true;
            //((WizardViewModel)this.DataContext).SeriesRefresh();
            //((WizardViewModel)this.DataContext).ExerciciosRefresh();
            this.Refresh = false;
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.ListPickerSerieSelectedIndex != null)
                lpkSeries.SelectedIndex = this.ListPickerSerieSelectedIndex;
        }


        //TODO: Achar uma forma melhor de tratar o bug do ListPicker que reseta o valor selecionado quando o modo de seleção é em modo de telea cheia (full mode)
        private void lpkExercicios_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Workaround para quando o listPicker selecionar um item em "full mode" (em tela cheia), selecionar o valor correto, pois quando ele carrega essa
            //tela novamente, a propriedade SelectedIndex é resetada
            if (e.AddedItems.Count > 0 && e.RemovedItems.Count > 0)
                this.ListPickerSerieSelectedIndex = new List<object>(((ListPicker)sender).ItemsSource.OfType<object>()).IndexOf(e.AddedItems[0]);
        }
        private void lpkSeries_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Workaround para quando o listPicker selecionar um item em "full mode" (em tela cheia), selecionar o valor correto, pois quando ele carrega essa
            //tela novamente, a propriedade SelectedIndex é resetada
            if (e.AddedItems.Count > 0 && !this.Refresh)
                this.ListPickerSerieSelectedIndex = new List<object>(((ListPicker)sender).ItemsSource.OfType<object>()).IndexOf(e.AddedItems[0]);
        }
    }
}