﻿/*
Copyright 2013 Rodrigo Cabral

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using AcademiaHelper.Resources;
using AcademiaHelper.Helpers;

namespace AcademiaHelper
{
    public partial class MainPage : PhoneApplicationPage
    {
        #region Appbar Events
        void MainPage_AppBar_HelpClick(object sender, EventArgs e)
        {
            MessageBox.Show("HelpClick");
        }

        void MainPage_AppBar_SettingClick(object sender, EventArgs e)
        {
            MessageBox.Show("Config");
        }

        void MainPage_AppBar_WizardClick(object sender, EventArgs e)
        {
            NavigationService.Navigate(Paginas.Wizard_Page1);
        }
        #endregion

        public MainPage()
        {
            InitializeComponent();

            //Settings for the application bar
            ApplicationBar = App.Current.Resources["MainPageAppBar"] as ApplicationBar;
            ((ApplicationBarIconButton)ApplicationBar.Buttons[0]).Click += MainPage_AppBar_WizardClick;
            ((ApplicationBarMenuItem)ApplicationBar.MenuItems[0]).Click += MainPage_AppBar_SettingClick;
            ((ApplicationBarMenuItem)ApplicationBar.MenuItems[1]).Click += MainPage_AppBar_HelpClick;
           

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            NavigationService.Navigate(Paginas.Wizard_Page1);

        }
    }
}