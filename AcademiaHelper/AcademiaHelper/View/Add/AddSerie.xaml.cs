﻿/*
Copyright 2013 Rodrigo Cabral

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using AcademiaHelper.Model;
using AcademiaHelper.Resources;
using AcademiaHelper.ViewModel;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System;
using System.Windows.Navigation;

namespace AcademiaHelper.View.Add
{
    public partial class AddSerie : PhoneApplicationPage
    {
        public AddSerie()
        {
            InitializeComponent();

            this.ApplicationBar = AppResourcesCustom.GenericAppBarForAddPages(SaveEvent: AppBar_btnSaveClick, CancelEvent: AppBar_btnCancelClick);
        }

        void AppBar_btnCancelClick(object sender, EventArgs e)
        {
            if (NavigationService.CanGoBack)
                NavigationService.GoBack();
        }

        void AppBar_btnSaveClick(object sender, EventArgs e)
        {
            //TODO: Implementar validação sobre a adição da série ao banco
            Serie serie = new Serie()
            {
                Nome = txtNome.Text,
                Descricao = txtDescricao.Text,
                Id = Guid.NewGuid()
            };

            SerieViewModel.AdicionarSerie(serie);

            if (NavigationService.CanGoBack)
                NavigationService.GoBack();
        }
    }
}