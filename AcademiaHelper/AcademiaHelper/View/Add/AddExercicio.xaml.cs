﻿/*
Copyright 2013 Rodrigo Cabral

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using AcademiaHelper.Model;
using AcademiaHelper.ViewModel;
using AcademiaHelper.Resources;

namespace AcademiaHelper.View.Add
{
    //TODO: Implementar cadastro de exercício com adicição de fotos
    public partial class AddExercicio : PhoneApplicationPage
    {
        public AddExercicio()
        {
            InitializeComponent();

            this.ApplicationBar = AppResourcesCustom.GenericAppBarForAddPages(SaveEvent: AppBar_btnSaveClick, CancelEvent: AppBar_btnCancelClick);
        }

        void AppBar_btnCancelClick(object sender, EventArgs e)
        {
            if (NavigationService.CanGoBack)
                NavigationService.GoBack();
        }

        void AppBar_btnSaveClick(object sender, EventArgs e)
        {
            //TODO: Implementar validação sobre a adição da exercício ao banco

            Exercicio exercicio = new Exercicio()
            {
                Id =  Guid.NewGuid(),
                Descricao = txtDescricao.Text,
                Nome = txtNome.Text,
                Instrucoes = txtInstrucoes.Text
            };

            ExercicioViewModel.AdicionarExercicio(exercicio);

            if (NavigationService.CanGoBack)
                NavigationService.GoBack();
        }

    }
}