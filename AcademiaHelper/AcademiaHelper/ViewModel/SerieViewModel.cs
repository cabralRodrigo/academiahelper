﻿/*
Copyright 2013 Rodrigo Cabral

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using AcademiaHelper.Helpers;
using AcademiaHelper.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AcademiaHelper.ViewModel
{
    public class SerieViewModel : ViewModelBase
    {
        /// <summary>
        /// Obtem um série por ID
        /// </summary>
        /// <param name="id">ID da série</param>
        /// <returns>Instância da classe Serie, nulo se não achou no banco</returns>
        public static Serie ObterPorID(Guid id)
        {
            Serie retorno;
            using (AcademiaHelperDBContext db = DBHelper.GetInstanceOfDatabase())
                retorno = db.Serie.Where(w => w.Id == id).FirstOrDefault();
            return retorno;
        }

        //TODO: Adicionar campo para ordenação das séries
        //TODO: Adicionar verificação para séries ativas ou não
        /// <summary>
        /// Obtem todas as séries ativas
        /// </summary>
        /// <returns>Lista com todas as séries ativas</returns>
        public static List<Serie> ObterTodasSeriesAtivas()
        {
            List<Serie> lstRetorno = new List<Serie>();
            using (AcademiaHelperDBContext db = DBHelper.GetInstanceOfDatabase())
                lstRetorno = db.Serie.ToList();


            return lstRetorno;
        }

        /// <summary>
        /// Adiciona uma série ao banco
        /// </summary>
        /// <param name="serie">Série a ser adicionada ao banco</param>
        public static void AdicionarSerie(Serie serie)
        {
            using (AcademiaHelperDBContext db = DBHelper.GetInstanceOfDatabase())
            {
                db.Serie.InsertOnSubmit(serie);
                db.SubmitChanges();
            }
        }
    }
}
