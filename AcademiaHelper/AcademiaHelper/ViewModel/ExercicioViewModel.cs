﻿using AcademiaHelper.Helpers;
using AcademiaHelper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademiaHelper.ViewModel
{
    public class ExercicioViewModel : ViewModelBase
    {
        /// <summary>
        /// Adiciona um exercício ao banco
        /// </summary>
        /// <param name="exercicio">Exercício a ser adicionado ao banco</param>
        public static void AdicionarExercicio(Exercicio exercicio)
        {
            using (AcademiaHelperDBContext db = DBHelper.GetInstanceOfDatabase())
            {
                db.Exercicio.InsertOnSubmit(exercicio);
                db.SubmitChanges();
            }
        }

        /// <summary>
        /// Obtem todos os exercícios
        /// </summary>
        /// <returns>Obtem todos os exercícios</returns>
        public static List<Exercicio> ObterTodosExercicios()
        {
            List<Exercicio> lstRetorno = new List<Exercicio>();
            using (AcademiaHelperDBContext db = DBHelper.GetInstanceOfDatabase())
                lstRetorno = db.Exercicio.OrderBy(o => o.Nome).ToList();

            return lstRetorno;
        }

    }
}
