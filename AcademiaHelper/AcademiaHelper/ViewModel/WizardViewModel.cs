﻿/*
Copyright 2013 Rodrigo Cabral

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using AcademiaHelper.Model;
using System;
using System.Collections.ObjectModel;

namespace AcademiaHelper.ViewModel
{
    public class WizardViewModel : ViewModelBase
    {
        private ObservableCollection<Serie> series;
        public ObservableCollection<Serie> Series
        {
            get
            {
                if (series == null)
                    SeriesRefresh();

                return series;
            }
            set
            {
                series = value;
                base.NotificarPropriedadeAlterada("Series");
            }
        }
        public void SeriesRefresh()
        {
            this.Series = new ObservableCollection<Serie>(SerieViewModel.ObterTodasSeriesAtivas());
        }

        private ObservableCollection<Exercicio> exercicios;
        public ObservableCollection<Exercicio> Exercicios
        {
            get
            {
                if (exercicios == null)
                    ExerciciosRefresh();
                return exercicios;
            }
            set
            {
                exercicios = value;
                base.NotificarPropriedadeAlterada("Exercicios");
            }
        }
        public void ExerciciosRefresh()
        {
            this.Exercicios = new ObservableCollection<Exercicio>(ExercicioViewModel.ObterTodosExercicios());
        }
    }
}
