
/*
Copyright 2013 Rodrigo Cabral

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#pragma warning disable 1591
namespace AcademiaHelper.Model
{
    using System.Data.Linq;
    using System.Data.Linq.Mapping;
    using System.Data;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Linq;
    using System.Linq.Expressions;
    using System.ComponentModel;
    using System;

    using System.IO;
    using System.IO.IsolatedStorage;
    using Microsoft.Phone.Data.Linq.Mapping;
    using Microsoft.Phone.Data.Linq;


    public class DebugWriter : TextWriter
    {
        private const int DefaultBufferSize = 256;
        private System.Text.StringBuilder _buffer;

        public DebugWriter()
        {
            BufferSize = 256;
            _buffer = new System.Text.StringBuilder(BufferSize);
        }

        public int BufferSize
        {
            get;
            private set;
        }

        public override System.Text.Encoding Encoding
        {
            get { return System.Text.Encoding.UTF8; }
        }

        #region StreamWriter Overrides
        public override void Write(char value)
        {
            _buffer.Append(value);
            if (_buffer.Length >= BufferSize)
                Flush();
        }

        public override void WriteLine(string value)
        {
            Flush();

            using (var reader = new StringReader(value))
            {
                string line;
                while (null != (line = reader.ReadLine()))
                    System.Diagnostics.Debug.WriteLine(line);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                Flush();
        }

        public override void Flush()
        {
            if (_buffer.Length > 0)
            {
                System.Diagnostics.Debug.WriteLine(_buffer);
                _buffer.Clear();
            }
        }
        #endregion
    }


    public partial class AcademiaHelperDBContext : System.Data.Linq.DataContext
    {

        public bool CreateIfNotExists()
        {
            bool created = false;
            if (!this.DatabaseExists())
            {
                string[] names = this.GetType().Assembly.GetManifestResourceNames();
                string name = names.Where(n => n.EndsWith(FileName)).FirstOrDefault();
                if (name != null)
                {
                    using (Stream resourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(name))
                    {
                        if (resourceStream != null)
                        {
                            using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                            {
                                using (IsolatedStorageFileStream fileStream = new IsolatedStorageFileStream(FileName, FileMode.Create, myIsolatedStorage))
                                {
                                    using (BinaryWriter writer = new BinaryWriter(fileStream))
                                    {
                                        long length = resourceStream.Length;
                                        byte[] buffer = new byte[32];
                                        int readCount = 0;
                                        using (BinaryReader reader = new BinaryReader(resourceStream))
                                        {
                                            // read file in chunks in order to reduce memory consumption and increase performance
                                            while (readCount < length)
                                            {
                                                int actual = reader.Read(buffer, 0, buffer.Length);
                                                readCount += actual;
                                                writer.Write(buffer, 0, actual);
                                            }
                                        }
                                    }
                                }
                            }
                            created = true;
                        }
                        else
                        {
                            this.CreateDatabase();
                            created = true;
                        }
                    }
                }
                else
                {
                    this.CreateDatabase();
                    created = true;
                }
            }
            return created;
        }

        public bool LogDebug
        {
            set
            {
                if (value)
                {
                    this.Log = new DebugWriter();
                }
            }
        }

        public static string FileName = AcademiaHelper.Helpers.Info.ACADEMIAHELPERDB_FILENAME;

        public AcademiaHelperDBContext(string connectionString)
            : base(connectionString)
        {
            OnCreated();
        }

        #region Extensibility Method Definitions
        partial void OnCreated();
        partial void InsertExercicio(Exercicio instance);
        partial void UpdateExercicio(Exercicio instance);
        partial void DeleteExercicio(Exercicio instance);
        partial void InsertRepeticao(Repeticao instance);
        partial void UpdateRepeticao(Repeticao instance);
        partial void DeleteRepeticao(Repeticao instance);
        partial void InsertSerie(Serie instance);
        partial void UpdateSerie(Serie instance);
        partial void DeleteSerie(Serie instance);
        #endregion

        public System.Data.Linq.Table<Exercicio> Exercicio
        {
            get
            {
                return this.GetTable<Exercicio>();
            }
        }

        public System.Data.Linq.Table<Repeticao> Repeticao
        {
            get
            {
                return this.GetTable<Repeticao>();
            }
        }

        public System.Data.Linq.Table<Serie> Serie
        {
            get
            {
                return this.GetTable<Serie>();
            }
        }
    }

    [global::System.Data.Linq.Mapping.TableAttribute()]
    public partial class Exercicio : INotifyPropertyChanging, INotifyPropertyChanged
    {

        private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);

        private System.Guid _Id;

        private string _Nome;

        private string _Descricao;

        private string _Instrucoes;

        private string _ArquivoFoto;

        private EntitySet<Repeticao> _Repeticao;

        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(System.Data.Linq.ChangeAction action);
        partial void OnCreated();
        partial void OnIdChanging(System.Guid value);
        partial void OnIdChanged();
        partial void OnNomeChanging(string value);
        partial void OnNomeChanged();
        partial void OnDescricaoChanging(string value);
        partial void OnDescricaoChanged();
        partial void OnInstrucoesChanging(string value);
        partial void OnInstrucoesChanged();
        partial void OnArquivoFotoChanging(string value);
        partial void OnArquivoFotoChanged();
        #endregion

        public Exercicio()
        {
            this._Repeticao = new EntitySet<Repeticao>(new Action<Repeticao>(this.attach_Repeticao), new Action<Repeticao>(this.detach_Repeticao));
            OnCreated();
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Name = "id", Storage = "_Id", DbType = "UniqueIdentifier NOT NULL", IsPrimaryKey = true)]
        public System.Guid Id
        {
            get
            {
                return this._Id;
            }
            set
            {
                if ((this._Id != value))
                {
                    this.OnIdChanging(value);
                    this.SendPropertyChanging();
                    this._Id = value;
                    this.SendPropertyChanged("Id");
                    this.OnIdChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Nome", DbType = "NVarChar(100) NOT NULL", CanBeNull = false)]
        public string Nome
        {
            get
            {
                return this._Nome;
            }
            set
            {
                if ((this._Nome != value))
                {
                    this.OnNomeChanging(value);
                    this.SendPropertyChanging();
                    this._Nome = value;
                    this.SendPropertyChanged("Nome");
                    this.OnNomeChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Descricao", DbType = "NVarChar(150)")]
        public string Descricao
        {
            get
            {
                return this._Descricao;
            }
            set
            {
                if ((this._Descricao != value))
                {
                    this.OnDescricaoChanging(value);
                    this.SendPropertyChanging();
                    this._Descricao = value;
                    this.SendPropertyChanged("Descricao");
                    this.OnDescricaoChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Instrucoes", DbType = "NVarChar(200)")]
        public string Instrucoes
        {
            get
            {
                return this._Instrucoes;
            }
            set
            {
                if ((this._Instrucoes != value))
                {
                    this.OnInstrucoesChanging(value);
                    this.SendPropertyChanging();
                    this._Instrucoes = value;
                    this.SendPropertyChanged("Instrucoes");
                    this.OnInstrucoesChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_ArquivoFoto", DbType = "NVarChar(50)")]
        public string ArquivoFoto
        {
            get
            {
                return this._ArquivoFoto;
            }
            set
            {
                if ((this._ArquivoFoto != value))
                {
                    this.OnArquivoFotoChanging(value);
                    this.SendPropertyChanging();
                    this._ArquivoFoto = value;
                    this.SendPropertyChanged("ArquivoFoto");
                    this.OnArquivoFotoChanged();
                }
            }
        }

        [global::System.Runtime.Serialization.IgnoreDataMember]
        [global::System.Data.Linq.Mapping.AssociationAttribute(Name = "FK_Repeticao_Exercicio", Storage = "_Repeticao", ThisKey = "Id", OtherKey = "Exercicio_ID", DeleteRule = "NO ACTION")]
        public EntitySet<Repeticao> Repeticao
        {
            get
            {
                return this._Repeticao;
            }
            set
            {
                this._Repeticao.Assign(value);
            }
        }

        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            if ((this.PropertyChanging != null))
            {
                this.PropertyChanging(this, emptyChangingEventArgs);
            }
        }

        protected virtual void SendPropertyChanged(String propertyName)
        {
            if ((this.PropertyChanged != null))
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void attach_Repeticao(Repeticao entity)
        {
            this.SendPropertyChanging();
            entity.Exercicio = this;
        }

        private void detach_Repeticao(Repeticao entity)
        {
            this.SendPropertyChanging();
            entity.Exercicio = null;
        }
    }

    [global::System.Data.Linq.Mapping.TableAttribute()]
    public partial class Repeticao : INotifyPropertyChanging, INotifyPropertyChanged
    {

        private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);

        private System.Guid _Id;

        private System.Guid _Serie_ID;

        private System.Guid _Exercicio_ID;

        private int _QuantidadeDeSeries;

        private int _RepeticoesPorSerie;

        private int _Ordem;

        private EntityRef<Exercicio> _Exercicio;

        private EntityRef<Serie> _Serie;

        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(System.Data.Linq.ChangeAction action);
        partial void OnCreated();
        partial void OnIdChanging(System.Guid value);
        partial void OnIdChanged();
        partial void OnSerie_IDChanging(System.Guid value);
        partial void OnSerie_IDChanged();
        partial void OnExercicio_IDChanging(System.Guid value);
        partial void OnExercicio_IDChanged();
        partial void OnQuantidadeDeSeriesChanging(int value);
        partial void OnQuantidadeDeSeriesChanged();
        partial void OnRepeticoesPorSerieChanging(int value);
        partial void OnRepeticoesPorSerieChanged();
        partial void OnOrdemChanging(int value);
        partial void OnOrdemChanged();
        #endregion

        public Repeticao()
        {
            this._Exercicio = default(EntityRef<Exercicio>);
            this._Serie = default(EntityRef<Serie>);
            OnCreated();
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Name = "id", Storage = "_Id", DbType = "UniqueIdentifier NOT NULL", IsPrimaryKey = true)]
        public System.Guid Id
        {
            get
            {
                return this._Id;
            }
            set
            {
                if ((this._Id != value))
                {
                    this.OnIdChanging(value);
                    this.SendPropertyChanging();
                    this._Id = value;
                    this.SendPropertyChanged("Id");
                    this.OnIdChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Serie_ID", DbType = "UniqueIdentifier NOT NULL")]
        public System.Guid Serie_ID
        {
            get
            {
                return this._Serie_ID;
            }
            set
            {
                if ((this._Serie_ID != value))
                {
                    if (this._Serie.HasLoadedOrAssignedValue)
                    {
                        throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
                    }
                    this.OnSerie_IDChanging(value);
                    this.SendPropertyChanging();
                    this._Serie_ID = value;
                    this.SendPropertyChanged("Serie_ID");
                    this.OnSerie_IDChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Exercicio_ID", DbType = "UniqueIdentifier NOT NULL")]
        public System.Guid Exercicio_ID
        {
            get
            {
                return this._Exercicio_ID;
            }
            set
            {
                if ((this._Exercicio_ID != value))
                {
                    if (this._Exercicio.HasLoadedOrAssignedValue)
                    {
                        throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
                    }
                    this.OnExercicio_IDChanging(value);
                    this.SendPropertyChanging();
                    this._Exercicio_ID = value;
                    this.SendPropertyChanged("Exercicio_ID");
                    this.OnExercicio_IDChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_QuantidadeDeSeries", DbType = "Int NOT NULL")]
        public int QuantidadeDeSeries
        {
            get
            {
                return this._QuantidadeDeSeries;
            }
            set
            {
                if ((this._QuantidadeDeSeries != value))
                {
                    this.OnQuantidadeDeSeriesChanging(value);
                    this.SendPropertyChanging();
                    this._QuantidadeDeSeries = value;
                    this.SendPropertyChanged("QuantidadeDeSeries");
                    this.OnQuantidadeDeSeriesChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_RepeticoesPorSerie", DbType = "Int NOT NULL")]
        public int RepeticoesPorSerie
        {
            get
            {
                return this._RepeticoesPorSerie;
            }
            set
            {
                if ((this._RepeticoesPorSerie != value))
                {
                    this.OnRepeticoesPorSerieChanging(value);
                    this.SendPropertyChanging();
                    this._RepeticoesPorSerie = value;
                    this.SendPropertyChanged("RepeticoesPorSerie");
                    this.OnRepeticoesPorSerieChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Ordem", DbType = "Int NOT NULL")]
        public int Ordem
        {
            get
            {
                return this._Ordem;
            }
            set
            {
                if ((this._Ordem != value))
                {
                    this.OnOrdemChanging(value);
                    this.SendPropertyChanging();
                    this._Ordem = value;
                    this.SendPropertyChanged("Ordem");
                    this.OnOrdemChanged();
                }
            }
        }

        [global::System.Runtime.Serialization.IgnoreDataMember]
        [global::System.Data.Linq.Mapping.AssociationAttribute(Name = "FK_Repeticao_Exercicio", Storage = "_Exercicio", ThisKey = "Exercicio_ID", OtherKey = "Id", IsForeignKey = true)]
        public Exercicio Exercicio
        {
            get
            {
                return this._Exercicio.Entity;
            }
            set
            {
                Exercicio previousValue = this._Exercicio.Entity;
                if (((previousValue != value)
                            || (this._Exercicio.HasLoadedOrAssignedValue == false)))
                {
                    this.SendPropertyChanging();
                    if ((previousValue != null))
                    {
                        this._Exercicio.Entity = null;
                        previousValue.Repeticao.Remove(this);
                    }
                    this._Exercicio.Entity = value;
                    if ((value != null))
                    {
                        value.Repeticao.Add(this);
                        this._Exercicio_ID = value.Id;
                    }
                    else
                    {
                        this._Exercicio_ID = default(System.Guid);
                    }
                    this.SendPropertyChanged("Exercicio");
                }
            }
        }

        [global::System.Runtime.Serialization.IgnoreDataMember]
        [global::System.Data.Linq.Mapping.AssociationAttribute(Name = "FK_Repeticao_Serie", Storage = "_Serie", ThisKey = "Serie_ID", OtherKey = "Id", IsForeignKey = true)]
        public Serie Serie
        {
            get
            {
                return this._Serie.Entity;
            }
            set
            {
                Serie previousValue = this._Serie.Entity;
                if (((previousValue != value)
                            || (this._Serie.HasLoadedOrAssignedValue == false)))
                {
                    this.SendPropertyChanging();
                    if ((previousValue != null))
                    {
                        this._Serie.Entity = null;
                        previousValue.Repeticao.Remove(this);
                    }
                    this._Serie.Entity = value;
                    if ((value != null))
                    {
                        value.Repeticao.Add(this);
                        this._Serie_ID = value.Id;
                    }
                    else
                    {
                        this._Serie_ID = default(System.Guid);
                    }
                    this.SendPropertyChanged("Serie");
                }
            }
        }

        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            if ((this.PropertyChanging != null))
            {
                this.PropertyChanging(this, emptyChangingEventArgs);
            }
        }

        protected virtual void SendPropertyChanged(String propertyName)
        {
            if ((this.PropertyChanged != null))
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [global::System.Data.Linq.Mapping.TableAttribute()]
    public partial class Serie : INotifyPropertyChanging, INotifyPropertyChanged
    {

        private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);

        private System.Guid _Id;

        private string _Nome;

        private string _Descricao;

        private EntitySet<Repeticao> _Repeticao;

        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(System.Data.Linq.ChangeAction action);
        partial void OnCreated();
        partial void OnIdChanging(System.Guid value);
        partial void OnIdChanged();
        partial void OnNomeChanging(string value);
        partial void OnNomeChanged();
        partial void OnDescricaoChanging(string value);
        partial void OnDescricaoChanged();
        #endregion

        public Serie()
        {
            this._Repeticao = new EntitySet<Repeticao>(new Action<Repeticao>(this.attach_Repeticao), new Action<Repeticao>(this.detach_Repeticao));
            OnCreated();
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Name = "id", Storage = "_Id", DbType = "UniqueIdentifier NOT NULL", IsPrimaryKey = true)]
        public System.Guid Id
        {
            get
            {
                return this._Id;
            }
            set
            {
                if ((this._Id != value))
                {
                    this.OnIdChanging(value);
                    this.SendPropertyChanging();
                    this._Id = value;
                    this.SendPropertyChanged("Id");
                    this.OnIdChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Nome", DbType = "NVarChar(100) NOT NULL", CanBeNull = false)]
        public string Nome
        {
            get
            {
                return this._Nome;
            }
            set
            {
                if ((this._Nome != value))
                {
                    this.OnNomeChanging(value);
                    this.SendPropertyChanging();
                    this._Nome = value;
                    this.SendPropertyChanged("Nome");
                    this.OnNomeChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Descricao", DbType = "NVarChar(200)")]
        public string Descricao
        {
            get
            {
                return this._Descricao;
            }
            set
            {
                if ((this._Descricao != value))
                {
                    this.OnDescricaoChanging(value);
                    this.SendPropertyChanging();
                    this._Descricao = value;
                    this.SendPropertyChanged("Descricao");
                    this.OnDescricaoChanged();
                }
            }
        }

        [global::System.Runtime.Serialization.IgnoreDataMember]
        [global::System.Data.Linq.Mapping.AssociationAttribute(Name = "FK_Repeticao_Serie", Storage = "_Repeticao", ThisKey = "Id", OtherKey = "Serie_ID", DeleteRule = "NO ACTION")]
        public EntitySet<Repeticao> Repeticao
        {
            get
            {
                return this._Repeticao;
            }
            set
            {
                this._Repeticao.Assign(value);
            }
        }

        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            if ((this.PropertyChanging != null))
            {
                this.PropertyChanging(this, emptyChangingEventArgs);
            }
        }

        protected virtual void SendPropertyChanged(String propertyName)
        {
            if ((this.PropertyChanged != null))
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void attach_Repeticao(Repeticao entity)
        {
            this.SendPropertyChanging();
            entity.Serie = this;
        }

        private void detach_Repeticao(Repeticao entity)
        {
            this.SendPropertyChanging();
            entity.Serie = null;
        }
    }
}
#pragma warning restore 1591
