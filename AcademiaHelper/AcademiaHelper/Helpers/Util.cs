﻿/*
Copyright 2013 Rodrigo Cabral

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Linq;

namespace AcademiaHelper.Helpers
{
    public static class Util
    {
        /// <summary>
        /// Aplica uma regra de entrada de texto em um TextBox, usar de preferência esse método no evento KeyUp
        /// </summary>
        /// <param name="txt">TextBox a ser aplicado a regra de entrada de texto</param>
        /// <param name="permitirDecimais">Diz se permite ou não decimais</param>
        /// <param name="permitirNegativo">Diz se permite ou não negativos</param>
        public static void FormatarEntradaDeNumeros(TextBox txt, bool permitirDecimais, bool permitirNegativo)
        {
            //Cria e adiciona os caracteres inválidos
            List<string> lstCaracteresInvalidos = new List<string>();
            lstCaracteresInvalidos.Add(",");
            lstCaracteresInvalidos.Add(" ");
            lstCaracteresInvalidos.Add("-");

            //Se não for permitido decimais, então adiciona '.'(ponto) como caractere inválido
            if (!permitirDecimais)
                lstCaracteresInvalidos.Add(".");

            //Retira todos os caracteres inválidos do TextBox
            foreach (string strCaracter in lstCaracteresInvalidos)
                txt.Text = txt.Text.Replace(strCaracter, "");

            //Posiciona o cursor do TextBox para o final, ao contrário ao mudar a propriedade Text, o cursor é deslocado para o inicio
            txt.Select(txt.Text.Length, 0);
        }
    }
}
