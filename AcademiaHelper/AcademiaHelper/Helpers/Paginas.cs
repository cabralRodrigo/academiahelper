﻿/*
Copyright 2013 Rodrigo Cabral

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;

namespace AcademiaHelper.Helpers
{
    public static class Paginas
    {
        public static Uri Add_Serie = ObterUri("/View/Add/AddSerie.xaml");
        public static Uri Add_Exercico = ObterUri("/View/Add/AddExercicio.xaml");

        public static Uri Wizard_Page1 = ObterUri("/View/Wizard/WizardPage1.xaml");

        /// <summary>
        /// Método que centraliza a criação de URI para ser usadas na navegação pelo app
        /// </summary>
        /// <param name="path">Caminho relativo do arquivo .xaml da página</param>
        /// <returns>Uri que aponta para a página</returns>
        private static Uri ObterUri(string path)
        {
            return new Uri(path, UriKind.Relative);
        }
    }
}
