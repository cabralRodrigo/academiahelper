﻿/*
Copyright 2013 Rodrigo Cabral

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AcademiaHelper.Model;

namespace AcademiaHelper.Helpers
{
    /// <summary>
    /// Class that contains all methods related to database
    /// </summary>
    public static class DBHelper
    {
        /// <summary>
        /// Gets an instance of AcademiaHelperDBContext, making sure that the database exists
        /// </summary>
        /// <returns>An instance of AcademiaHelperDBContext</returns>
        public static AcademiaHelperDBContext GetInstanceOfDatabase()
        {
            //Instance context with the default connection string
            AcademiaHelperDBContext db = new Model.AcademiaHelperDBContext(Info.ACADEMIAHELPERDB_CONNECTIONSTRING);

            //If the database does not physically exist, then create
            if (!db.DatabaseExists())
                db.CreateDatabase();

            return db;
        }
    }
}
