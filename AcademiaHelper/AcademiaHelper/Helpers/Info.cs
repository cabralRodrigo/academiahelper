﻿/*
Copyright 2013 Rodrigo Cabral

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademiaHelper.Helpers
{
    /// <summary>
    /// Class that contains some information used throughout the project
    /// </summary>
    public static class Info
    {
        /// <summary>
        /// Physical path of the file from the database
        /// </summary>
        public const string ACADEMIAHELPERDB_FILENAME = "AcademiaHelperDB.sdf";

        /// <summary>
        /// Connection string for the databse
        /// </summary>
        public const string ACADEMIAHELPERDB_CONNECTIONSTRING = "isostore:/" + ACADEMIAHELPERDB_FILENAME;
    }
}
