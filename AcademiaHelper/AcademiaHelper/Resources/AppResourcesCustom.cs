﻿/*
Copyright 2013 Rodrigo Cabral

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using Microsoft.Phone.Shell;
using System;

namespace AcademiaHelper.Resources
{

    /// <summary>
    /// Classe que contém resources customizados
    /// </summary>
    public class AppResourcesCustom
    {
        /// <summary>
        /// Cria um AppBar genérico para telas de add.
        /// Esse recurso era pra estar nos resources dentro do app.xaml, porém o windows phone cria uma instância para cada resource e ao adicionar métodos ao evento click
        /// dos botões da AppBar e então quando esse resource era usado mais de uma vez, acontecia de o evento ser chamado mais de uma vez
        /// </summary>
        /// <param name="SaveEvent">Método que será associado ao click do botão Save</param>
        /// <param name="CancelEvent">Método que será associado ao click do botão Cancel</param>
        /// <returns></returns>
        public static IApplicationBar GenericAppBarForAddPages(EventHandler SaveEvent = null, EventHandler CancelEvent = null)
        {
            ApplicationBarIconButton btnSave = new ApplicationBarIconButton()
            {
                IconUri = new Uri("/Assets/AppBar/save.png", UriKind.Relative),
                IsEnabled = true,
                Text = AppResources.AddPages_AppBar_SaveButton
            };
            if (SaveEvent != null)
                btnSave.Click += SaveEvent;

            ApplicationBarIconButton btnCancel = new ApplicationBarIconButton()
            {
                IconUri = new Uri("/Assets/AppBar/cancel.png", UriKind.Relative),
                IsEnabled = true,
                Text = AppResources.AddPages_AppBar_CancelButton                
            };
            if (CancelEvent != null)
                btnCancel.Click += CancelEvent;

            ApplicationBar appBar = new ApplicationBar();
            appBar.Buttons.Add(btnSave);
            appBar.Buttons.Add(btnCancel);

            return appBar;
        }
    }
}
